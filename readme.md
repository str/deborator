# deborator readme

deborator (DEBug decORATOR) is a TypeScript decorator, to add console.log() to all methods and properties of a TypeScript class.

If you find yourself writing a lot of console.log() entries during debugging, then deborator may save you time!
 
This can be especially useful when regular debugging via breakpoints is not really practical

examples:
- developing drag'n'drop features in a browser
    since browsers like Chrome tend not to exit the 'dragging' state, if they hit a breakpoint

- developing real-time sites, when debugging via breakpoints is not practical

note: deborator should NOT be deployed to Production (as there may be performance/security impact because of the logging)

## usage

Install deborator via yarn:

```
yarn add deborator
```

Import deborator into the TypeScript file, and apply the decorator to the class you want to debug:

```
import { deborator } from "deborator";

@deborator({})
class MyCalculator {
    constructor(private value: number) {}

    add(value: number) {
        this.value += value;
    }
}

// using your class:
const calc = new MyCalculator(1);
calc.add(5);
```

Now if you hit F12, you will see console log entries for your class, as it is being used:

```
>DB> new MyCalculator( 1 )
>DB> MyCalculator.add( 5 )
>DB>   MyCalculator.add( 5 ) => ( {"value":6} )
```

### advanced usage

deborator options: (all are optional)

| option | description | example |
|---|---|---|
|log| A function that accepts a string (a message generated by deborator). This overrides the default behaviour (logging to console).| ```@deborator({log: (text: string) => {console.info(text);} }) ``` |
|showReturnValues| Set this to true, if you also want to see return values in the log.| ```@deborator({showReturnValues: true }) ``` |
|showProperties| Set this to true, to also log get/set properties. This can be noisy!. | ```@deborator({showProperties: true }) ``` |
|decoratePrototype| Set this to true, to also log inherited methods (does not work with React components).| ```@deborator({decoratePrototype: true }) ``` |

### known issues

- import into react project - right now need to do this (import source rather than compiled js):
    ```import { deborator } from "deborator/src/deborator";```
- dist folder (npm package): the index.js does not work from one unit test project
    workaround: copy the source file 'deborator.ts' into the project

## running the demo app

To see deborator in action, you can get the source code and run the demo app.

- get the source code:

```
git clone https://bitbucket.org/str/deborator
```

### dependencies

- node [v6.10.3 is OK]
- yarn [0.27.5 is OK]

### setup

On a Windows box (Unix should be similar ...)

```
CD demo-app
install
```

### run the app

```
run
```

## relevant links

sourcecode in git:
https://bitbucket.org/str/deborator

npm package:
https://www.npmjs.com/package/deborator

TypeScript decorators:
https://www.typescriptlang.org/docs/handbook/decorators.html

https://gist.github.com/remojansen/16c661a7afd68e22ac6e

### demo app

The demo app was based on a really nice react-typescript starter kit by Jack Franklin

https://github.com/javascript-playground/react-typescript-jest-demo

https://javascriptplayground.com/blog/2017/04/react-typescript/

## license

MIT

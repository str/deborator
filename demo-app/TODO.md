# deborator TODO

DONE - figure out a way to prevent deploy to production
    - could add warning to console ...

DONE - add option to take a callback, to override the default logging (by default deborator logs to console)
DONE - document the options in the readme and republish to npm
DONE - use the callback in the demo app, to log to the page AND console

DONE - add tests for get/set properties

- add tests for more complex methods

- import into react project - right now need to do this:
    import { deborator } from "deborator/src/deborator";
- dist folder (npm package): the index.js does not work from one unit test project
    workaround: copy the source file 'deborator.ts' into the project
    fix: npm pub needs tsc with *commonjs* module kind? (for node_modules resolution)

OPTIONAL (new project?)
- add contract checks: non-null, number ranges (@contract)

REM CALL build.bat

PUSHD dist
copy /y ..\ts-build\deborator.js index.js
copy /y ..\src\deborator.ts src\deborator.ts
copy /y ..\..\readme.md

CALL npm version patch
CALL npm publish

POPD

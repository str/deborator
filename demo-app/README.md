# demo app

based on app skeleton by Jack Franklin.

## React + TypeScript + Jest demo app

- `yarn install`
- `yarn start`
- `open http://localhost:3000`

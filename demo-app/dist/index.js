class DeboratorOptions {
    constructor() {
        this.showReturnValues = false;
        this.showProperties = false;
    }
}
/**
 * deborator - Debug Decorator
 *
 * @param target - the class that is being decorated
 */
export function deborator(options) {
    // tslint:disable-next-line:no-console
    console.warn("deborator is active - NOT suitable for use in Production! (since console logging may impact performance and security)");
    return (target) => {
        // save a reference to the original constructor
        const original = target;
        let instance = null;
        const log = (text) => {
            const message = ">DB> " + text;
            if (options.log) {
                options.log(message);
            }
            else {
                // tslint:disable-next-line:no-console
                console.log(message);
            }
        };
        const abbreviate = (text) => {
            text = text ? text : "";
            const maxChars = 20;
            if (text.length > maxChars) {
                text = text.substr(0, maxChars) + "...";
            }
            return text;
        };
        const argToString = (arg) => {
            let text = "";
            try {
                text = JSON.stringify(arg, null, 0);
            }
            catch (ex1) {
                try {
                    text = arg.toString();
                }
                catch (ex2) {
                    text = "[unprintable]";
                }
            }
            return text;
        };
        const dumpArgs = (args) => {
            const argsList = new Array();
            args.forEach(arg => {
                argsList.push(abbreviate(argToString(arg)));
            });
            const argsText = argsList.join(", ");
            return argsText.length > 0 ? "( " + argsText + " )" : "()";
        };
        const dumpReturnValue = (value) => {
            const format = (text) => {
                return "[" + text + "]";
            };
            const typeName = typeof value;
            if (typeName === "undefined") {
                return format(typeName);
            }
            if (!typeName) {
                return format("null");
            }
            return dumpArgs([value]);
        };
        let FunctionKind;
        (function (FunctionKind) {
            FunctionKind[FunctionKind["Normal"] = 0] = "Normal";
            FunctionKind[FunctionKind["Getter"] = 1] = "Getter";
            FunctionKind[FunctionKind["Setter"] = 2] = "Setter";
        })(FunctionKind || (FunctionKind = {}));
        const decorateFunction = (c, prop) => {
            const overrideFunction = (fun, kind) => {
                return (...args) => {
                    let funName = prop;
                    switch (kind) {
                        case FunctionKind.Normal:
                        default:
                            break;
                        case FunctionKind.Getter:
                            funName = "get " + funName;
                            break;
                        case FunctionKind.Setter:
                            funName = "set " + funName;
                            break;
                    }
                    const callMessage = original.name + "." + funName + dumpArgs(args);
                    log(callMessage);
                    const returnValue = fun.apply(c, args);
                    if (options.showReturnValues) {
                        log("  " + callMessage + " => " + dumpReturnValue(returnValue));
                    }
                    if (kind !== FunctionKind.Setter) {
                        return returnValue;
                    }
                };
            };
            const originalFun = c[prop];
            if (typeof originalFun !== "function") {
                let getter = c.__lookupGetter__(prop);
                let setter = c.__lookupSetter__(prop);
                if (options.showProperties) {
                    if (typeof (getter) === "function") {
                        c.__defineGetter__(prop, overrideFunction(getter, FunctionKind.Getter));
                    }
                    if (typeof (setter) === "function") {
                        c.__defineSetter__(prop, overrideFunction(setter, FunctionKind.Setter));
                    }
                }
            }
            else {
                c[prop] = overrideFunction(originalFun, FunctionKind.Normal);
            }
        };
        // a utility function to generate instances of a class,
        // with the constructor and each function wrapped with a logging function.
        // tslint:disable-next-line:ban-types
        const construct = (constructor, args) => {
            const c = function () {
                return constructor.apply(this, args);
            };
            c.prototype = constructor.prototype;
            instance = new c();
            // decorate the class functions
            // - trying to avoid decorating sub-class or other decorators such as mobx functions
            const protoPropNames = Object.getOwnPropertyNames(c.prototype);
            const propsDone = [];
            for (let prop in protoPropNames) {
                prop = protoPropNames[prop];
                if (prop !== "constructor" && c.prototype.hasOwnProperty(prop)) {
                    decorateFunction(instance, prop);
                    propsDone.push(prop);
                }
            }
            // decorate the instance functions - to cover lambda-style member functions
            for (const prop in instance) {
                if (propsDone.indexOf(prop) < 0 && instance.hasOwnProperty(prop)) {
                    decorateFunction(instance, prop);
                }
            }
            return instance;
        };
        const decoratingConstructor = (...args) => {
            log("new " + original.name + dumpArgs(args));
            return construct(original, args);
        };
        // copy prototype so instanceof operator still works
        decoratingConstructor.prototype = original.prototype;
        // return new constructor (will wrap original)
        return decoratingConstructor;
    };
}
//# sourceMappingURL=deborator.js.map
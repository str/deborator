declare class DeboratorOptions {
    log?: (text: string) => void;
    showReturnValues?: boolean;
    showProperties?: boolean;
    decoratePrototype?: boolean;
}

/**
 * deborator - Debug Decorator
 *
 * @param target - the class that is being decorated
 */
declare function deborator(options: DeboratorOptions);

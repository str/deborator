import { deborator } from "./deborator";

const outputMessage = (text: string) => {
    // tslint:disable-next-line:no-console
    console.log(text);
};

// advanced usage: overriding the default logging behaviour:
let logProxy: ((text: string) => void) | null = null;

const logCaller = (text: string) => {
    if (logProxy) {
        logProxy(text);
    }
};

//@deborator({log: logCaller})
//@deborator({log: logCaller, showProperties: true})
//@deborator({log: logCaller, showReturnValues: true, showProperties: true})
@deborator({log: logCaller, showReturnValues: true, decoratePrototype: true, showProperties: true})
class Calculator {
    constructor(private innerValue: number) { }

    add = (...args: any[]): Calculator => {
        args.forEach((arg) => {
            if (isFinite(arg)) {
                this.innerValue += arg;
            }
        });
        return this;
    }

    multiply(value: number): Calculator {
        if (isFinite(value)) {
            this.innerValue *= value;
        }
        return this;
    }

    toString(): string {
        return this._getValueAsString();
    }

    get value(): number {
        return this.innerValue;
    }

    set value(value: number) {
        this.innerValue = value;
    }

    // TODO more complex methods
    // TODO contract checks?: non-null, number ranges

    private _getValueAsString(): string {
        return this.innerValue.toString();
    }
}

// tslint:disable-next-line:max-classes-per-file
export class DeboratorDemo {
    static run(log:(text: string) => void) {
        logProxy = log;

        const debTest = new Calculator(1);
        debTest.add(5);
        debTest.add(2, 3, 0);
        debTest.add(null);
        debTest.add(undefined);
        debTest.add(10).multiply(2);
        const calcSummary = "(1 + 2 + 3 + 5 + 10) * 2";
        outputMessage(calcSummary + " = " + debTest.value);
        outputMessage(calcSummary + " = " + debTest);
        debTest.value = 666;
        outputMessage(calcSummary + " = " + debTest.value);        
    }
}

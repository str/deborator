import React from "react";
import ReactDOM from "react-dom";

import {DeboratorDemo} from "./deborator-demo";

const App = () => {

  // override the default logging behaviour, so we can see the output on-page:
  let buffer = "";
  const logger = (text: string) => {
    buffer += text + "\n";

    console.info(text);
  };

  DeboratorDemo.run(logger); 

  return (
    <div>
      <b>deborator demo app</b>
      <p>Press F12 to see the console log from deborator!</p>
      <br/>
      code using a class 'Calculator' which uses deborator:
      <pre>
        const debTest = new Calculator(1);<br/>
        debTest.add(5);<br/>
        debTest.add(2, 3, 0);<br/>
        debTest.add(null);<br/>
        debTest.add(undefined);<br/>
        debTest.add(10).multiply(2);<br/>
        ...<br/>
      </pre>
      <br/>
      deborator output:
      <pre>
        {buffer}
        </pre>
    </div>
  );
};

ReactDOM.render( <App />, document.getElementById("app"));

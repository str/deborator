class DeboratorOptions {
    log?: (text: string) => void;
    showReturnValues? = false;
    showProperties? = false;
    decoratePrototype? = false;
}

/**
 * deborator - Debug Decorator
 *
 * @param target - the class that is being decorated
 */
export function deborator(options: DeboratorOptions) {
    // tslint:disable-next-line:no-console
    console.warn("deborator is active - NOT suitable for use in Production! (since console logging may impact performance and security)");

    return (target: any) => {
        // save a reference to the original constructor
        const original = target;

        let instance = null;

        const log = (text: string) => {
            const message = ">DB> " + text;

            if(options.log) {
                options.log(message);
            } else {
                // tslint:disable-next-line:no-console
                console.log(message);
            }
        };

        const abbreviate = (text: string): string => {
            text = text ? text : "";
            const maxChars = 20;
            if (text.length > maxChars) {
                text = text.substr(0, maxChars) + "...";
            }

            return text;
        };

        const argToString = (arg: any): string => {
            let text = "";

            try {
                text = JSON.stringify(arg, null, 0);
            } catch (ex1) {
                try {
                    text = arg.toString();
                } catch (ex2) {
                    text = "[unprintable]";
                }
            }

            return text;
        };

        const dumpArgs = (args: any[]): string => {
            const argsList = new Array<string>();
            args.forEach(arg => {
                argsList.push(abbreviate(argToString(arg)));
            });

            const argsText = argsList.join(", ");

            return argsText.length > 0 ? "( " + argsText + " )" : "()";
        };

        const dumpReturnValue = (value: any) => {
            const format = (text: string) => {
                return "[" + text + "]";
            };

            const typeName = typeof value;
            if (typeName === "undefined") {
                return format(typeName);
            }
            if (!typeName) {
                return format("null");
            }
            return dumpArgs([value]);
        };

        enum FunctionKind {
            Normal,
            Getter,
            Setter
        }

        const decorateFunction = (c: any, prop: string) => {
            const overrideFunction = (fun: Function, kind: FunctionKind): Function => {
                return (...args: any[]) => {
                    let funName = prop;
                    switch(kind) {
                        case FunctionKind.Normal:
                        default:
                        break;
                        case FunctionKind.Getter:
                            funName = "get " + funName;
                        break;
                        case FunctionKind.Setter:
                            funName = "set " + funName;
                        break;
                    }

                    const callMessage = original.name + "." + funName + dumpArgs(args);
                    log(callMessage);
    
                    const returnValue = fun.apply(c, args);
    
                    if (options.showReturnValues) {
                        log("  " + callMessage + " => " + dumpReturnValue(returnValue));
                    }
    
                    if (kind !== FunctionKind.Setter) {
                        return returnValue;
                    }
                }
            };

            const originalFun = c[prop];
            if (typeof originalFun !== "function") {

                let getter = c.__lookupGetter__(prop);
                let setter = c.__lookupSetter__(prop);

                if(options.showProperties) {
                    if (typeof(getter) === "function") {
                        c.__defineGetter__(prop, overrideFunction(getter, FunctionKind.Getter));
                    }
                    if (typeof(setter) === "function") {
                        c.__defineSetter__(prop, overrideFunction(setter, FunctionKind.Setter));
                    }
                }
            } else {
                c[prop] = overrideFunction(originalFun, FunctionKind.Normal);
            }
        };

        // a utility function to generate instances of a class,
        // with the constructor and each function wrapped with a logging function.
        // tslint:disable-next-line:ban-types
        const construct = (constructor: Function, args: any[]) => {
            const c: any = function(this: any) {
                return constructor.apply(this, args);
            };
            c.prototype = constructor.prototype;

            instance = new c();

            // decorate the class functions
            // - trying to avoid decorating sub-class or other decorators such as mobx functions
            const protoPropNames = Object.getOwnPropertyNames(c.prototype);
            const propsDone: string[] = [];
            if (options.decoratePrototype) {
                for (let prop in protoPropNames) {
                    prop = protoPropNames[prop];
                    if (prop !== "constructor" && c.prototype.hasOwnProperty(prop)) {
                        decorateFunction(instance, prop);
                        propsDone.push(prop);
                    }
                }
            }

            // decorate the instance functions - to cover lambda-style member functions
            for (const prop in instance) {
                if (propsDone.indexOf(prop) < 0 && instance.hasOwnProperty(prop)) {
                    decorateFunction(instance, prop);
                }
            }

            return instance;
        };

        const decoratingConstructor: any = (...args: any[]) => {
            log("new " + original.name + dumpArgs(args));
            return construct(original, args);
        };

        // copy prototype so instanceof operator still works
        decoratingConstructor.prototype = original.prototype;

        // return new constructor (will wrap original)
        return decoratingConstructor;
    };
}
